package main

import (
	"fmt"
	"golang-vlad/cache"
	"time"
)

func main() {
	cache := cache.New(time.Second)
	cache.Set("userId", 42)
	userId, _ := cache.Get("userId")
	fmt.Println(userId)
	cache.Delete("userId")
	userId, _ = cache.Get("userId")
	fmt.Println(userId)
}
