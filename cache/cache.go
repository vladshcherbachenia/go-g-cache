package cache

import (
	"sync"
	"time"
)

type value struct {
	value any
	ttl   *time.Time
}

type Cache struct {
	ticker *time.Ticker
	data   sync.Map
	ttl    time.Duration
}

func (db *Cache) Set(key any, v any) {
	t := time.Now().Add(db.ttl)
	db.data.Store(key, &value{v, &t})
}

func (db *Cache) Get(key any) (result any, ok bool) {
	load, ok := db.data.Load(key)
	if !ok {
		return nil, false
	}

	vv, ok := load.(*value)
	if !ok {
		return nil, false
	}

	return vv.value, true
}

func (db *Cache) Delete(key any) {
	db.data.Delete(key)
}

func New(ttl time.Duration) *Cache {
	db := &Cache{
		ticker: time.NewTicker(time.Second * 1),
		data:   sync.Map{},
		ttl:    ttl,
	}

	return db
}
